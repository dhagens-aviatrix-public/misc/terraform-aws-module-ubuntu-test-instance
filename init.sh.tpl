#! /bin/bash
sudo hostnamectl set-hostname ${name}
sleep 15m #Allow internet access to be established. E.g. IGW or NGFW instantiation.
sudo apt-get update
sudo apt-get install -y apache2
sudo systemctl start apache2
sudo systemctl enable apache2
echo "<h1>${name}</h1>" | sudo tee /var/www/html/index.html