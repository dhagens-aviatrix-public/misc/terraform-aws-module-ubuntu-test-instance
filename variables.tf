variable "name" {
  type = string
}

variable "ubuntu_image" {
  type    = string
  default = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
}

/*
variable "pub_dns_suffix" {
    type = string
}

variable "priv_dns_suffix" {
    type = string
}
*/

variable "subnet_id" {
  type = string
}

variable "eth1_subnet_id" {
  type    = string
  default = ""
}

variable "ssh_key_name" {
  type = string
}

variable "security_groups" {
}

variable "instance_size" {
  type    = string
  default = "t2.micro"
}

variable "pub_ip" {
  type    = bool
  default = false
}

variable "eip" {
  type    = bool
  default = false
}

variable "eth1" {
  type    = bool
  default = false
}

variable "user_data" {
  default = ""
}

variable "source_dest_check" {
  type    = bool
  default = true
}

locals {
  ud = length(var.user_data) > 0 ? var.user_data : data.template_file.init.rendered
}